package br.com.estoque;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiEstoqueH2Application {

	public static void main(String[] args) {
		SpringApplication.run(ApiEstoqueH2Application.class, args);
	}

}
