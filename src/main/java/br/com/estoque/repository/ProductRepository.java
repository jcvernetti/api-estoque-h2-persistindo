package br.com.estoque.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.estoque.model.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {

}
